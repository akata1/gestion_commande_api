<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/client' => [[['_route' => 'client', '_controller' => 'App\\Controller\\ClientController::index'], null, null, null, true, false, null]],
        '/clients' => [[['_route' => 'getAllClients', '_controller' => 'App\\Controller\\ClientController::getAll'], null, ['GET' => 0], null, false, false, null]],
        '/client/add' => [[['_route' => 'add_client', '_controller' => 'App\\Controller\\ClientController::addClient'], null, ['POST' => 0], null, false, false, null]],
        '/commande' => [[['_route' => 'commande', '_controller' => 'App\\Controller\\CommandeController::index'], null, null, null, false, false, null]],
        '/commandes' => [[['_route' => 'commandes', '_controller' => 'App\\Controller\\CommandeController::getAllCommande'], null, ['GET' => 0], null, false, false, null]],
        '/product' => [[['_route' => 'product', '_controller' => 'App\\Controller\\ProductController::index'], null, null, null, false, false, null]],
        '/products' => [[['_route' => 'allProducts', '_controller' => 'App\\Controller\\ProductController::getAllProducts'], null, ['GET' => 0], null, false, false, null]],
        '/product/add' => [[['_route' => 'addProduct', '_controller' => 'App\\Controller\\ProductController::addProduct'], null, ['POST' => 0], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_error/(\\d+)(?:\\.([^/]++))?(*:35)'
                .'|/c(?'
                    .'|lient/(?'
                        .'|([^/]++)(*:64)'
                        .'|delete/([^/]++)(*:86)'
                        .'|update/([^/]++)(*:108)'
                    .')'
                    .'|ommande/(?'
                        .'|([^/]++)(*:136)'
                        .'|update/([^/]++)(*:159)'
                        .'|delete/([^/]++)(*:182)'
                    .')'
                .')'
                .'|/product/(?'
                    .'|([^/]++)(*:212)'
                    .'|update/([^/]++)(*:235)'
                    .'|delete/([^/]++)(*:258)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        35 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        64 => [[['_route' => 'get_one_client', '_controller' => 'App\\Controller\\ClientController::get'], ['id'], ['GET' => 0], null, false, true, null]],
        86 => [[['_route' => 'delete_one_client', '_controller' => 'App\\Controller\\ClientController::deleteClient'], ['id'], ['GET' => 0], null, false, true, null]],
        108 => [[['_route' => 'update_one_client', '_controller' => 'App\\Controller\\ClientController::updateClient'], ['id'], ['PUT' => 0], null, false, true, null]],
        136 => [[['_route' => 'commande_one', '_controller' => 'App\\Controller\\CommandeController::getOneCommande'], ['id'], ['GET' => 0], null, false, true, null]],
        159 => [[['_route' => 'add', '_controller' => 'App\\Controller\\CommandeController::update'], ['id'], ['PUT' => 0], null, false, true, null]],
        182 => [[['_route' => 'app_commande_delete', '_controller' => 'App\\Controller\\CommandeController::delete'], ['id'], null, null, false, true, null]],
        212 => [[['_route' => 'one_product', '_controller' => 'App\\Controller\\ProductController::getOneProduct'], ['id'], ['GET' => 0], null, false, true, null]],
        235 => [[['_route' => 'update', '_controller' => 'App\\Controller\\ProductController::update'], ['id'], ['PUT' => 0], null, false, true, null]],
        258 => [
            [['_route' => 'deleteProduct', '_controller' => 'App\\Controller\\ProductController::deleteClient'], ['id'], ['GET' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
