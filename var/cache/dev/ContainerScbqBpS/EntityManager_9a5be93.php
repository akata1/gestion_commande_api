<?php

namespace ContainerScbqBpS;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolderb107b = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer294ea = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties452fe = [
        
    ];

    public function getConnection()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getConnection', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getMetadataFactory', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getExpressionBuilder', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'beginTransaction', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getCache', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getCache();
    }

    public function transactional($func)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'transactional', array('func' => $func), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->transactional($func);
    }

    public function commit()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'commit', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->commit();
    }

    public function rollback()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'rollback', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getClassMetadata', array('className' => $className), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'createQuery', array('dql' => $dql), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'createNamedQuery', array('name' => $name), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'createQueryBuilder', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'flush', array('entity' => $entity), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'clear', array('entityName' => $entityName), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->clear($entityName);
    }

    public function close()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'close', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->close();
    }

    public function persist($entity)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'persist', array('entity' => $entity), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'remove', array('entity' => $entity), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'refresh', array('entity' => $entity), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'detach', array('entity' => $entity), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'merge', array('entity' => $entity), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getRepository', array('entityName' => $entityName), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'contains', array('entity' => $entity), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getEventManager', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getConfiguration', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'isOpen', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getUnitOfWork', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getProxyFactory', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'initializeObject', array('obj' => $obj), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'getFilters', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'isFiltersStateClean', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'hasFilters', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return $this->valueHolderb107b->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer294ea = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolderb107b) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolderb107b = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolderb107b->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, '__get', ['name' => $name], $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        if (isset(self::$publicProperties452fe[$name])) {
            return $this->valueHolderb107b->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderb107b;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderb107b;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderb107b;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderb107b;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, '__isset', array('name' => $name), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderb107b;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderb107b;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, '__unset', array('name' => $name), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderb107b;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderb107b;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, '__clone', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        $this->valueHolderb107b = clone $this->valueHolderb107b;
    }

    public function __sleep()
    {
        $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, '__sleep', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;

        return array('valueHolderb107b');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer294ea = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer294ea;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer294ea && ($this->initializer294ea->__invoke($valueHolderb107b, $this, 'initializeProxy', array(), $this->initializer294ea) || 1) && $this->valueHolderb107b = $valueHolderb107b;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderb107b;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderb107b;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
