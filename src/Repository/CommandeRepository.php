<?php

namespace App\Repository;

use App\Entity\Commande;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Commande|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commande|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commande[]    findAll()
 * @method Commande[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandeRepository extends ServiceEntityRepository
{

    private $manager;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Commande::class);
        $this->manager = $manager;
    }


    // Save command
    public function saveCommande($numPro,$numCli,$qte){
        $newCommande = new Commande();

        $newCommande
        ->setNumProduit($numPro)
        ->setNumClient($numCli)
        ->setQte($qte);

        $this->manager->persist($newCommande);
        $this->manager->flush();
    }


        // Delete Commande

    public function deleteCommande(Commande $commande){
        $this->manager->remove($commande);
        $this->manager->flush();
    }

    // update commande
    public function updateCommande(Commande $commmade,$commandes){
        empty($commandes['numProduit']) ? true : $commmade->setNumProduit($commandes['numProduit']);
        empty($commandes['numClient']) ? true : $commmade->setNumClient($commandes['numClient']);
        empty($commandes['qte']) ? true : $commmade->setQte($commandes['qte']);

        $this->manager->flush();
    }
    // /**
    //  * @return Commande[] Returns an array of Commande objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Commande
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
