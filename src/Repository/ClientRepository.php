<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    private $manager;
    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $manager)
    {
        parent::__construct($registry, Client::class);
         $this->manager = $manager;
    }

    public function saveClient($codeClient, $nomClient)
    {
        $newClient = new Client();

        $newClient
            ->setCodeClient($codeClient)
            ->setNomClient($nomClient);
        $this->manager->persist($newClient);
        $this->manager->flush();
    }


    public function deleteClient(Client $client)
    {
        $this->manager->remove($client);
        $this->manager->flush();
    }

    public function updateClient(Client $client, $data){
        empty($data['codeClient']) ? true : $client->setCodeClient($data['codeClient']);
        empty($data['nomClient']) ? true : $client->setNomClient($data['nomClient']);

        $this->manager->flush();
    }
    // /**
    //  * @return Client[] Returns an array of Client objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Client
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
