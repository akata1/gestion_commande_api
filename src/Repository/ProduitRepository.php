<?php

namespace App\Repository;

use App\Entity\Produit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Produit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produit[]    findAll()
 * @method Produit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitRepository extends ServiceEntityRepository
{
    private $manager;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Produit::class);
        $this->manager = $manager;
    }

    //Save Product
    public function saveProduit($numPro,$design,$pu){
        $newProduit = new Produit();
        $newProduit
        ->setNumProduit($numPro)
        ->setDesign($design)
        ->setPu($pu);

        $this->manager->persist($newProduit);
        $this->manager->flush();


    }

    // Delete Product

    public function deleteProduct(Produit $product){
        $this->manager->remove($product);
        $this->manager->flush();
    }

    // Update product

    public function updateProduct(Produit $product,$products){
        empty($products['numProduit']) ? true : $product->setNumProduit($products['numProduit']);
        empty($products['Design']) ? true : $product->setDesign($products['Design']);
        empty($products['Pu']) ? true : $product->setPu($products['Pu']);

        $this->manager->flush();
    }
    // /**
    //  * @return Produit[] Returns an array of Produit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Produit
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
