<?php

namespace App\Entity;

use App\Repository\ProduitRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProduitRepository::class)
 */
class Produit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numProduit;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Design;

    /**
     * @ORM\Column(type="integer")
     */
    private $Pu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumProduit(): ?string
    {
        return $this->numProduit;
    }

    public function setNumProduit(string $numProduit): self
    {
        $this->numProduit = $numProduit;

        return $this;
    }

    public function getDesign(): ?string
    {
        return $this->Design;
    }

    public function setDesign(string $Design): self
    {
        $this->Design = $Design;

        return $this;
    }

    public function getPu(): ?int
    {
        return $this->Pu;
    }

    public function setPu(int $Pu): self
    {
        $this->Pu = $Pu;

        return $this;
    }
}
