<?php

namespace App\Controller;

use App\Entity\Client;
use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ClientController extends AbstractController
{

    private $clientRepository;
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @Route("/client/", name="client")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to Client API',
        ]);
    }

    /**
     * @Route("/clients", name="getAllClients", methods={"GET"})
     */
    public function getAll(): JsonResponse{

        $clients = $this->clientRepository->findAll();
        $data = [];
        
        foreach ($clients as $client) {
            $data[] = [
                'id' => $client->getId(),
                'codeClient' => $client->getCodeClient(),
                'nomClient' => $client->getNomClient()
            ];
        }
        return new JsonResponse(['clients'=> $data], 200);
    }
    /**
     * @Route("/client/add", name="add_client", methods={"POST"})
     */
    public function addClient(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $codeClient = $data['codeClient'];
        $nomClient = $data['nomClient'];

        if (empty($codeClient) || empty($nomClient)) {
            throw new NotFoundHttpException('Create failed!');
        }

        $this->clientRepository->saveClient($codeClient, $nomClient);

        return new JsonResponse(['status' => 'Client created!'], Response::HTTP_CREATED);
    }
        /**
         * @Route("/client/{id}", name="get_one_client", methods={"GET"})
         */
        public function get($id): JsonResponse
        {
            $client = $this->clientRepository->findOneBy(['id' => $id]);

            $data = [
                'id' => $client->getId(),
                'codeClient' => $client->getCodeClient(),
                'nomClient' => $client->getNomClient()
            ];

            return new JsonResponse(["client"=>$data], Response::HTTP_OK);
        }

        /**
         * @Route("/client/delete/{id}", name="delete_one_client", methods={"GET"})
         */

         public function deleteClient($id): JsonResponse
         {
            $response = [
                "response" => "Suppression avec succès"
            ];
            $client = $this->clientRepository->findOneBy(['id'=> $id]);
            $this->clientRepository->deleteClient($client);
            return new JsonResponse(["response"=>$response], Response::HTTP_OK);
         }
        /**
         * @Route("/client/update/{id}", name="update_one_client", methods={"PUT"})
         */

         public function updateClient($id, Request $request): JsonResponse
         {
             $response = [
                "response" => "Modification avec succès"
            ];
            $client = $this->clientRepository->findOneBy(['id' => $id]);
            $data = json_decode($request->getContent(), true);
            $this->clientRepository->updateClient($client,$data);
            return new JsonResponse(["response"=>$response],Response::HTTP_OK);
         }

}
