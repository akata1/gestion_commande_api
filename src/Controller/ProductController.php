<?php

namespace App\Controller;

use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class ProductController extends AbstractController
{

    private $productRepository;

    public function __construct(ProduitRepository $repo)
    {
        
        $this->productRepository = $repo;
    }
    /**
     * @Route("/product", name="product")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to product API!'
        ]);
    }

    /**
     * @Route("/products",name="allProducts", methods={"GET"})
     */

     public function getAllProducts(): JsonResponse
     {
        $products = $this->productRepository->findAll();

        $data = [];

        foreach ($products as $product) {
            $data[] = [
                'id' => $product->getId(),
                'numProduct' => $product->getNumProduit(),
                'Design' => $product->getDesign(),
                'Pu' => $product->getPu()
            ];

        }

        return new JsonResponse(["products" => $data], Response::HTTP_OK);
    }

    /**
     * @Route("/product/add",name="addProduct", methods={"POST"} )
     */

     public function addProduct(Request $body): JsonResponse
     {
         $data = json_decode($body->getContent(), true);
         $num = $data['numProduct'];
         $design = $data['Design'];
         $pu = $data['Pu'];

         if (empty($num) || empty($design) || empty($pu)) {
            throw new NotFoundHttpException('Create failed!');
         }

         $this->productRepository->saveProduit($num,$design,$pu);
         $response = [
             "response" => "Ajout avec success"
         ];
         return new JsonResponse(["response"=>$response], Response::HTTP_OK);
     }

        /**
         * @Route("/product/{id}", name="one_product", methods={"GET"})
         */
        public function getOneProduct($id): JsonResponse
        {
            $commande = $this->productRepository->findOneBy(['id' => $id]);

            $data = [
                'id' => $commande->getId(),
                'numProduct' => $commande->getNumProduit(),
                'Design' => $commande->getDesign(),
                'Pu' => $commande->getPu()
            ];

            return new JsonResponse(["product"=>$data], Response::HTTP_OK);
        }


        /**
         * @Route("product/update/{id}", name="update", methods={"PUT"})
         */

         public function update($id, Request $request): JsonResponse{
             $response = [
                "response" => "Modification avec succès"
            ];
            $product = $this->productRepository->findOneBy(['id' => $id]);
            $data = json_decode($request->getContent(), true);
            $this->productRepository->updateProduct($product,$data);
            return new JsonResponse(["response"=>$response],Response::HTTP_OK);
         }

        /**
         * @Route("/product/delete/{id}", name="deleteProduct", methods={"GET"})
         */

         public function deleteClient($id): JsonResponse
         {
            $response = [
                "response" => "Suppression avec succès"
            ];
            $product = $this->productRepository->findOneBy(['id'=> $id]);
            $this->productRepository->deleteProduct($product);
            return new JsonResponse(["response"=>$response], Response::HTTP_OK);
         }
}
