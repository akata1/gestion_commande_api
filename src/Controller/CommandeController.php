<?php

namespace App\Controller;

use App\Repository\CommandeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CommandeController extends AbstractController
{

    private $commandeRepository;

    public function __construct(CommandeRepository $repo)
    {
        $this->commandeRepository = $repo;
    }
    /**
     * @Route("/commande", name="commande")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to commande API!'
        ]);
    }

    /**
     * @Route("/commandes", name="commandes", methods={"GET"})
     */
    public function getAllCommande(): JsonResponse{
        $commandes = $this->commandeRepository->findAll();

        $data = [];

        foreach ($commandes as $commande) {
            $data[] = [
                'id' => $commande->getId(),
                'numProduit' => $commande->getNumProduit(),
                'numClient' => $commande->getNumClient(),
                'qte' => $commande->getQte()
            ];

        }
        return new JsonResponse(["commandes"=>$data], Response::HTTP_OK);
    }

        /**
         * @Route("/commande/{id}", name="commande_one", methods={"GET"})
         */
        public function getOneCommande($id): JsonResponse
        {
            $commande = $this->commandeRepository->findOneBy(['id' => $id]);

            $data = [
                'id' => $commande->getId(),
                'numClient' => $commande->getNumClient(),
                'numProdui' => $commande->getNumProduit(),
                'qte' => $commande->getQte()
            ];

            return new JsonResponse(["commande"=>$data], Response::HTTP_OK);
        }

        /**
         * @Route("commande/add", name="add", methods={"POST"})
         */
         public function add(Request $request): JsonResponse{

            $body= json_decode($request->getContent(), true);

            $numCli = $body['numClient'];
            $numPro = $body['numProduit'];
            $qte = $body['qte'];
            if (empty($numCli) || empty($numPro) || empty($qte)) {
            throw new NotFoundHttpException('Create failed!');
         }
            $this->commandeRepository->saveCommande($numPro,$numCli,$qte);
            $response = [
             "response" => "Ajout avec success"
         ];
         return new JsonResponse(["response"=>$response], Response::HTTP_OK);
         }


         /**
         * @Route("commande/update/{id}", name="add", methods={"PUT"})
         */
          public function update($id, Request $request): JsonResponse{
           $response = [
                "response" => "Modification avec succès"
            ];
            $commande = $this->commandeRepository->findOneBy(['id' => $id]);
            $data = json_decode($request->getContent(), true);
            $this->commandeRepository->updateCommande($commande,$data);
            return new JsonResponse(["response"=>$response],Response::HTTP_OK);
          }


        /**
         * @Route("/commande/delete/{id}")
         */
        public function delete($id): JsonResponse{
             $response = [
                "response" => "Suppression avec succès"
            ];
            $commande = $this->commandeRepository->findOneBy(['id'=> $id]);
            $this->commandeRepository->deleteCommande($commande);
            return new JsonResponse(["response"=>$response], Response::HTTP_OK);
        }
}
